{lib,config, pkgs, ... }:
  {

  imports =
    [ 
     # ./hardware-configuration.nix
     ./vangrd.nix
    ];


  systemd.extraConfig=''DefaultTimeoutStopSec=10s'';
  services.journald.extraConfig=''SystemMaxUse=100M'';

  console = {
     font = "Lat2-Terminus16";
     keyMap = "de";
  };

  i18n = {
     defaultLocale = "en_US.UTF-8";
     supportedLocales = ["en_US.UTF-8/UTF-8" "de_DE.UTF-8/UTF-8"];
     extraLocaleSettings = {LC_TIME = "de_DE.UTF-8";};
   };

  # Set your time zone.
  time.timeZone = "Europe/Amsterdam";

  environment.systemPackages = with pkgs; [
    vim tmux 
    mc sudo manpages 
  ];

 programs.vim.defaultEditor = true;

 networking.firewall = { 
   enable = true;
   allowPing = true;
   rejectPackets = true;
 };


 nixpkgs.config = {
  allowUnfree = true;
 };

   system.stateVersion = "20.03";
}
