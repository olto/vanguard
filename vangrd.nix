#
{config, pkgs, ... }:
{
  imports =
    [ 
     ./tinc.nix
     ./wireguard.nix
    ];


  networking.hostName = "vangrd"; # Define your hostname.

  # sshd 
  services.openssh = {
    enable=true;
    openFirewall = true;
    ports = [ 54091 ];
    permitRootLogin = "no";
    startWhenNeeded = false;
  };

  documentation.enable = false;
  services.journald.extraConfig="SystemMaxUse=500M";

  

  users.users.olto = {
  isNormalUser = true;
  home = "/home/olto";
  description = "intials";
  extraGroups = [ "wheel" ];
  hashedPassword ="$6$pI98KNvzRjuMBk.$zD.E9DrY2YbNLnjyZhCuzVJusS5C0QDgLxOnrJZn6wHRw7w9a0prnt8wgo6vi2wkVQAB.LH6Iza89ii0y9CqX1";
  openssh.authorizedKeys.keys = [ "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIC8U6bKYFj6M/yV4Pd7aq9JRkLxBe2rsxzZzML3sZI3x olto@x6f74" ];
};


  fonts.fonts = with pkgs;[ 
    terminus_font
    hack-font
    iosevka
    corefonts #microsoft free fonts
  ];

  fonts.fontconfig = {
   defaultFonts.monospace = [ "hack" "terminus" "iosevka-thin" ];
   defaultFonts.sansSerif = [ "corefonts"];
   defaultFonts.serif = [ "Liberation Serif"];
  };

  nixpkgs.config = {
    allowUnfree = true;
    packageOverrides = pkgs: {
      unstable = import <nixos-unstable> {
        # pass the nixpkgs config to the unstable alias
        # to ensure `allowUnfree = true;` is propagated:
        config = config.nixpkgs.config;
      };
    };
  };
  

   environment.systemPackages = with pkgs; [
   #net
   wget curl 
   #packer
   i7z unzip zip unrar
   #vpn
   tinc_pre wireguard 
   ];


 #garbage disposal 
 nix.gc.automatic = true;
 nix.gc.dates = "weekly";
 nix.gc.options = "--delete-older-than 30d";
 nix.allowedUsers = [ "@wheel" ];

}
