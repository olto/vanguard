# vanguard
Nixos base config for vanguard server

## Install on Hetzner cloud 
 acc. https://gist.github.com/nh2/c02612e05d1a0f5dc9fd50dda04b3e48

 ```
set -e

mkfs.ext4 /dev/sda1

mount /dev/sda1 /mnt

nixos-generate-config --root /mnt

# add /mnt/etc/nixos/configuration.nix
imports = [./base.nix ];
boot.loader.grub.devices = [ "/dev/sda" ];
```

## fetch basic nix files
```
curl https://gitlab.com/olto/vanguard/-/raw/master/base.nix > /mnt/etc/nixos/base.nix
curl https://gitlab.com/olto/vanguard/-/raw/master/vangrd.nix > /mnt/etc/nixos/vangrd.nix
```

## install and reboot

```
# set root passwd
nixos-install
shutdown -h now

```

# Hetzner Konsole
 Eject NIXOS ISO-Image @ hetzner cloud and restart server


# Random Stuff
### Flask
Pictures not loading ?!? remedy  `chmod -R nginx:nginx /srv/msc` 
### ACME
 *  add in `vangrd.nix` infos about ACME
 *  need the serving directory to be writable by nginx (look under #flask)





